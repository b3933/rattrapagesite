// function to change image on index page
window.onload = function () {

    // we create an image table with image links
    let images = ['./images/barniere_1.jpg', './images/barniere_2.jpg', './images/barniere.jpg'];

    let index = 0;
    // we select the image we want to change each 5s
    const imgElement = document.querySelector('#banniere_image');
    console.log(imgElement);



    function change() {

        imgElement.style.backgroundImage = `url(${images[index]})`;
        index > 1 ? index = 0 : index++;
    }

    setInterval(change, 5000);
};