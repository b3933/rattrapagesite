
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style_index.css" />
        <title>Zou de Bricou</title>
        
        <script src="./ChangeHome_Image.js">  </script>
        
    
    </head>

    <style>

        div.gallery {
            margin: 5px;
            border: 1px solid #ccc;
            float: left;
            width: 291px;
        }

        div.gallery:hover {
            transform: scale(1.3);
            border: 1px solid #777;
        }

        div.gallery im {
            width: 100%;
            height: 200px;
        }

        div.desc {
            padding: 15px;
            text-align: center;


        }
    </style>

    <body>
        <div id="bloc_page">
            <header>
                <div id="titre_principal">
                    <div id="logo">
                         <img src="images/logo.png" alt="Logo" />
                        
                        <h1>Zou de Bricou</h1>-
                    </div>
                    <h2>presenté par Bricou : fondateur de Zou</h2>
                </div>
                
                <nav>

                    <ul>
                        <li class="active"><a href="index.php">Accueil</a></li>

                        <li><a href="./authentification.php">Se connecter</a></li>
                        <li><a href="inscription.html">S'inscrire</a></li>

                    </ul>
                </nav>
            </header>
            
            <div id="banniere_image">
                <div id="banniere_description">
                    Le top 50 des zoos à visiter dans le monde selon TripAdvisor...
                    <a href="#" class="bouton_rouge">Voir l'article <img src="images/flecheblanchedroite.png" alt="" /></a>
                </div>
            </div>
            
            <section>
                <article>
                    <h1><img src="images/ico_epingle.png" alt="Catégorie voyage" class="ico_categorie" />Bienvenue !</h1>
                    <p>J'ai toujours été  passionné par les animaux  et les zoos. En  2005, j'ai eu
                        l'occasion de travailler au zoo d'Amnéville. En 2008, le zoo du Pal dans
                        l'Allier m'a accueilli. Le travail au sein de ces espaces zoologiques a ac-
                        centué ma passion, et m'a conduit à visiter divers parcs  zoologiques en
                        France, en Allemagne, en Suisse, en République Tchèque et en Belgique.
                        Chacune de mes visites est l'occasion de découvrir de nouvelles espèces,
                        de nouvelles structures et enrichissements d'enclos, et de compléter ma
                        collection de guides zoologiques. .
                        Mes visites sont une précieuse occasion de recueillir des informations sur
                        le mode de vie des animaux, mais également de prendre de nombreuses
                        photos des différentes espèces et installations.

                        J'ai choisi de créer ce site, afin de vous faire partager ma passion et de vous
                        faire découvrir les parcs zoologiques et les différentes espèces qui y vivent.
                        Zoo de Bricou vous propose de découvrir les différentes espèces animales en suivant
                        la classification scientifique.</p>
            

                </article>
                
                    <aside>
                    <h1><br>Bienvenue chez nous !</h1>
                    <img src="images/bulle.png" alt="" id="fleche_bulle" />
                    <p id="photo"><img src="images/anonyme.png" /></p>
                    <p>Je me présente&nbsp;. Je m'appelle Bricou, je suis le fondateur du Zou.</p>
                    <p><img src="images/facebook.png" alt="Facebook" /><img src="images/twitter.png" alt="Twitter" /><img src="images/rss.png" alt="RSS" /></p>
                </aside>
               


            </section>
            <section>
                <article>

                <h1><img src="images/ico_epingle.png" alt="Catégorie voyage" class="ico_categorie" />Secteur du Zou </h1>

            <div class="gallery">
                <a target="_blank" href="im/aquarium.jpg">
                    <img src="im/aquarium.jpg"  width="125" height="100">
                </a>
                <div class="desc">Secteur 1 : Aquarium  <br> Venez découvrir l'aquarium <br> Surface Area: 2000m2 <br> Number of Animals : 230  
                </div>
            </div>

            <div class="gallery">
                <a target="_blank" href="im/ciel.jpg">
                    <img src="im/ciel.jpg" width="125" height="100">
                </a>
                <div class="desc">Secteur 2 : Ciel  <br> Venez découvrir le ciel <br> Surface Area: 2000m2 <br> Number of Animals : 54 
                </div>
            </div>

            <div class="gallery">
                <a target="_blank" href="im/foret.png">
                    <img src="im/foret.png" alt="Northern Lights" width="125" height="100">
                </a>
                <div class="desc">Secteur 3 : Foret <br> Venez découvrir la foret<br> Surface Area: 2000m2 <br> Number of Animals : 75 
                </div>
            </div>

            <div class="gallery">
                <a target="_blank" href="im/jungle.jpg">
                    <img src="im/jungle.jpg" alt="Mountains" width="125" height="100">
                </a>
                <div class="desc">Secteur 4 : Jungle <br> Venez découvrir la jungle <br> Surface Area: 2000m2 <br> Number of Animals : 49 
                </div>
            </div>

          

                </article>
            </section>
            <footer>
                <div id="tweet">
                    <h1>Prévision météo</h1>
                    <p>Aujourd'hui il fera:</p>
                    <p>21°c</p>
                </div>
                <div id="mes_photos">
                    <h1>Gallerie Photos</h1>
                    <p><img src="images/photo2.jpg" alt="Photographie" /><img src="images/photo3.jpg" alt="Photographie" /><img src="images/photo4.jpg" alt="Photographie" /></p>
                </div>
                <div id="mes_amis">
                    <h1>Liens Utiles</h1>
                    <div id="listes_amis">
                        <ul>
                            <li><a href="#">Taureau</a></li>
                            <li><a href="#">Vache</a></li>
                            <li><a href="#">Pingouin</a></li>
                            <li><a href="#">Moustique</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Belette</a></li>
                            <li><a href="#">Concombre de mer</a></li>
                            <li><a href="#">Lion</a></li>
                            <li><a href="#">Tigre</a></li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </body>
  <center> <p>&copy; 2022 Bricou ZOU . All Rights Reserved.</p></center>
</html>