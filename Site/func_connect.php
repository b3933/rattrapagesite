<?php

function saveUSer($login){
    $_SESSION["userLogin"] = $login;
}

function isUserConnected(){
    $res = FALSE;
    if(isset($_SESSION["userLogin"])){
        if($_SESSION["userLogin"] != ""){
            $res = TRUE;
        }
    }
    return $res;
}

function forgetUser(){
    $_SESSION["userLogin"] = "";
    session_unset();
}

session_start();

?>
